---
title: "Recurring developers meetup: how and what"
date: 2023-01-15
draft: false
---

From 2023 this project will have a recurring meeting: **on the 15th of each month**, regardless of the day of the week or time zone, **at 18:00 CET**.

This is the [first developer meeting](https://mobilizon.libr.events/events/754072e2-3fd7-4e01-aae6-a73b0f11e706) event. To participate remotely, join our [matrix channel](https://matrix.to/#/#mobilize.berlin:systemli.org), a jitsi link will be shared before the event.

A report should be posted on the website the days after the meeting.

<!--more-->

The meeting is online, as it should be attended by people in different time zones. If some of the participants are in the same city, they can connect from the same space.

### Agenda points

1. Fix the upload of images. It is necessary to download the main image of an event and publish it.
2. Fix the HTML sent to mobilizon, because it is dirty from the metadata at the end of the post.
3. Tell about the event at HIP on the fediverse, and try to embed material from the fediverse into this site (made on [hugo](https://gohugo.io)).
4. Design a unit test for a robust date extraction.

{{<figure
  src="/images/libr.rocket.png"
  link="https://mobilizon.libr.events/events/754072e2-3fd7-4e01-aae6-a73b0f11e706"
>}}

<!--
Also, about the HIP talk, there is not yet a video online, but https://streaming.media.ccc.de/jev22/relive/49208 this was the full livestream, and joseph and I are at minute 18:40
-->
