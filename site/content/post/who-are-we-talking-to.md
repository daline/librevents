---
title: "Who are we talking to"
description: "Target audience analysis for mobilize.berlin and how libr.events fits in the picture"
date: 2022-09-01
draft: false
toc: true
---

We had the oppotunity to reflect on the complexity of our message. We are talking to four different actors: **Facebook tolerant**, the **Facebook critics**, the **Advocate** and the **Followers**. See below to know more.

<!--more-->

## 1) the "REALISTS" (or Facebook tolerant)

they would ideally leave Facebook because they see the probblem but they have to pay the rent for their location and get most of their crowd over Facebook.
They will continue on Facebook but want to support alternative platforms and why not migrate at some point. They don't have the time and resources to post twice on Facebook and then on Mobilize.Berlin

The "realists" might also have difficulty understanding how decentralization, and the Fediverse work (usability of the technology). To them the Fediverse seems less frequented, has an uglier design and might seem like many efforts for a small reach.

We can offer an easy "two clicks" option to automatically "scrape" (or copy, or share) their events from Facebook into Mobilize.Berlin: [**librevents**](/concept) TOOL.

They then have two possibilities:

1. **Other people scrape & republish your events**: allow the scraping of their events by a bot. But they have no control on their account on Mobilize.Berlin. The name of their page on Mobilize.Berlin will be called `unofficial` _Name_of_account_. They can ask the bot to be suspended at any time.

2. **You scrape and republish your events**: they create an account on Mobilize.Berlin and configure the extension so use it. They can then benefit from the Mobilizon features, such as the integration in the Fediverse, group chats, update the information, answer to comments, etc...

## 2) the CONVERTED (or Facebook critics)

they have already left facebook, because they don't support targeted advertising, stealing of personal data and all what the GAFAM stand for. They might though be interested to reshare their events on other platforms (if not on facebook, maybe twitter, telegram or another platform).

We want to help this crowd with another tool: the [**RESHARE**](/mobilizon-reshare) TOOL. They need an account on any mobilizon installation, and `mobilizon-reshare` would pull it and repost them. Can also be seen as a free version of itfff with more appropriate configuration settings.

## 3) THE ADVOCATES (you, likely)

They would like every person to abandon Facebook, they are willing to make additional effort, accepts the lack of content moderation because it is seen as a symptom of greater freedom (perhaps not applicable to all contexts), and accepts less developed, localized, and integrated tools than the mainstream, because the freedom of free software and the self-determination of federated networks are more important values.

They will engage in using and promoting libr.events (contribute to the scraping through their facebook page? how can they be active, apart from advocating), mobilizon-reshared and the use of mobilizon itself.

## 4) The "MASS" (or the followers)

They dont see the problem with Facebook, we need to adress them specifically. The mass, being is a passiv, follower crowd, they might join Mobilizon when it becomes fancy/hype.

---

To offer you context, this is a problem definition:

## What [Mobilize.berlin](https://mobilize.berlin) needs

Although many event managers support Mobilize Berlin and would love to stop using corporate data-mining services like Facebook Events, they also want to promote their events to as many possible concert and party-goers as possible. At the moment, this means promoting their events on multiple platforms at the same time.

This research was part of the documentary [DISAPPEAR](https://www.imdb.com/title/tt16227608/), that part start at 0:22:28, then the documentary detour around the world of self-determination online, and the announcement of the initiative happens at 1:14:20 👇

{{<youtube-privacy image="/images/F5xwZ0olSqA.jpg" id="F5xwZ0olSqA" >}}

#### What is the issue with the migration?

After the numerous scandals that have shown the true face of Facebook and the GAFAM (Google Amazon Facebook Apple Twitter), many people have been looking for alternatives. Many are for example unhappy to use Facebook to promote or look for events. The software Mobilizon and our platform mobilize.berlin was launched last year in order to help these event organizers (clubs, concert, live music).

We though face the problem of the "critical mass": the billions of users and main asset of Facebook that make many of us "hostages" of our own friends' presence on Facebook.

Although we now have better networks thanks to the Fediverse (1), we lack this mass of adopters. Therefor we have identified different audiences of event organizers and are working on tools to help their migration and those of their users.

### Who are we?

We are a group of developers, artists, digital rights advocates who want to quit Facebook and other internet giants. We want to help other people do the migration to more ethical, more transparent and community-owned platforms.

We are a small but dedicated group of Berlin-based hackers & Free Software advocates, artists, journalists, creators, and so on. We want to support the music, club and party scene by providing a privacy-friendly event-management platform run and managed by our own community.

One year ago [we launched Mobilize Berlin](https://blogs.fsfe.org/joseph/2021/06/20/speech-about-community-free-software-decentralization-and-mobilize-berlin/), a community-driven platform to publish events based on the Free Software Mobilizon and part of the Fediverse*.

