---
title: mobilizon-poster
date: 2022-11-17
draft: false
layout: page
---

# What is `mobilizon-poster` ?

A nodejs package meant to support scripts to login, submit, update, delete events on mobilizon.

```
npm install @vecna/_mobilizon-poster
```

Please check the [npmjs](https://www.npmjs.com/package/@_vecna/mobilizon-poster), and the [github](https://github.com/vecna/mobilizon-poster#readme) page.


# Usage as executable scripts

In this example you see how the login is performed. The variables `email`, `password`, and `api` are read by [nconf](https://www.npmjs.com/package/nconf), therefore can be loaded via environment variables, config file, or long options like below:

```
~/D/mobilizon-poster$ DEBUG=* bin/login.js --email mobilibr@mt2015.com --password experiment --api https://mobilizon.libr.events/api
  bin:login retrieved authentication token!  +0ms
  bin:login Loaded existing 1 identities, servers: [https://mobilizon.libr.events/api] +815ms
  bin:login Saving token and account info in /tmp/.identities.json file; this would be used as default +1ms
Saved authentication token in /tmp/.identities.json. Servers supported: [https://mobilizon.libr.events/api]
```

Once you performed login in mobilizon, you can use the `poster.js` script. If executed without option, it return this error:

```
~/D/mobilizon-poster$ DEBUG=* bin/poster.js 

--start missing!		When the event begin: Format YYYY-MM-DD HH:mm

Required options:		start,end,title,description,url,address

To provide variables like start, you have three ways: --longopt, environment, or config.json
You can check out the documentation here: https://libr.events/mobilizon-poster
Or fill up a bug report: https://github.com/vecna/mobilizon-poster/issues
```

Below there is a complete execution of `poster.js`; the `DEBUG=*` is intentionally verbose.

```
~/D/mobilizon-poster$ DEBUG=* bin/poster.js \
     --start "2022-11-19 10:11" \
     --title "ciao ciao" \
     --description "blah[...]." \ 
     --address "Lisbon, LX Factory" \
     --end "2022-11-19 22:22" \
     --url "https://ciaociaociao.it"
  mobi:shared Returning server from settings https://mobilizon.libr.events/api +0ms
  mobi:location Picking token from:  local cache +0ms
  mobi:shared Returning a token (343 bytes) registered on 2022-11-17T19:58:24.883Z +1ms
  mobi:shared Connecting to https://mobilizon.libr.events/api +0ms
  mobi:location Fetched 1 possible locations, the first is: {
  mobi:location   "__typename": "Address",
  mobi:location   "country": "Portugal",
  mobi:location   "description": "LXFactory",
  mobi:location   "geom": "-9.17841241671848;38.7021442",
  mobi:location   "id": null,
  mobi:location   "locality": "Lisbon",
  mobi:location   "originId": "nominatim:221960606",
  mobi:location   "postalCode": "1300-501",
  mobi:location   "region": null,
  mobi:location   "street": " ",
  mobi:location   "type": "locality",
  mobi:location   "url": null
  mobi:location } +1s
  mobi:location (acceptable) error in saving location cache: S is not defined undefined +0ms
  bin:poster {
  bin:poster   start: Moment<2022-11-19T10:11:00+00:00>,
  bin:poster   end: Moment<2022-11-19T22:22:00+00:00>,
  bin:poster   title: 'ciao ciao',
  bin:poster   description: 'ehochwefuhwrfuiwrfhuwehuih23u4ih23uih3u2ih4i32 h23u4h3u2i4h ui234ui234i 23g 4ui23gui423giu4234gui23 g4u23 g4iu2 g4i2g 4ig2 u4ig23 i4gui234gui 234i3 g423g4ui 23g4i3g4i 24iu234i42 g4ui23g4 23giu4',
  bin:poster   url: 'https://ciaociaociao.it',
  bin:poster   address: 'Lisbon, LX Factory',
  bin:poster   location: {
  bin:poster     __typename: 'Address',
  bin:poster     country: 'Portugal',
  bin:poster     description: 'LXFactory',
  bin:poster     geom: '-9.17841241671848;38.7021442',
  bin:poster     id: null,
  bin:poster     locality: 'Lisbon',
  bin:poster     originId: 'nominatim:221960606',
  bin:poster     postalCode: '1300-501',
  bin:poster     region: null,
  bin:poster     street: ' ',
  bin:poster     type: 'locality',
  bin:poster     url: null
  bin:poster   }
  bin:poster } +0ms
  mobi:shared Returning organizer Id 12 +1s
  mobi:poster Picking token from:  local cache +0ms
  mobi:shared Returning a token (343 bytes) registered on 2022-11-17T19:58:24.883Z +0ms
  mobi:poster Successful event created: {"id":79,"createdUUID":"1216f47e-da3d-4f6c-a1ec-86f165a760cc","eventURL":"https://mobilizon.libr.events/events/1216f47e-da3d-4f6c-a1ec-86f165a760cc","eventvars":{"start":"2022-11-19T10:11:00.000Z","end":"2022-11-19T22:22:00.000Z","title":"ciao ciao","description":"ehochwefuhwrfuiwrfhuwehuih23u4ih23uih3u2ih4i32 h23u4h3u2i4h ui234ui234i 23g 4ui23gui423giu4234gui23 g4u23 g4iu2 g4i2g 4ig2 u4ig23 i4gui234gui 234i3 g423g4ui 23g4i3g4i 24iu234i42 g4ui23g4 23giu4","url":"https://ciaociaociao.it","address":"Lisbon, LX Factory","location":{"__typename":"Address","country":"Portugal","description":"LXFactory","geom":"-9.17841241671848;38.7021442","id":null,"locality":"Lisbon","originId":"nominatim:221960606","postalCode":"1300-501","region":null,"street":" ","type":"locality","url":null}}} +1s
Event created at: https://mobilizon.libr.events/events/1216f47e-da3d-4f6c-a1ec-86f165a760cc
  bin:poster Posted successfully, now listing the last four Event in the node: +1s
{
  "data": {
    "events": {
      "elements": [
        {
          "beginsOn": "2022-11-19T10:11:00Z",
          "description": "ehochwefuhwrfuiwrfhuwehuih23u4ih23uih3u2ih4i32 h23u4h3u2i4h ui234ui234i 23g 4ui23gui423giu4234gui23 g4u23 g4iu2 g4i2g 4ig2 u4ig23 i4gui234gui 234i3 g423g4ui 23g4i3g4i 24iu234i42 g4ui23g4 23giu4",
          "endsOn": "2022-11-19T22:22:00Z",
          "id": "79",
          "picture": null,
          "title": "ciao ciao",
          "url": "https://mobilizon.libr.events/events/1216f47e-da3d-4f6c-a1ec-86f165a760cc"
        }
      ],
      "total": 1
    }
  }
}
~/D/mobilizon-poster$ 
```

And this created in the mobilizon instance and event like this:
                              
{{<figure src="/images/mobi-poster/mobi-poster-result.png">}}

If you look carefully, there is a numeric ID associated to the event created above (`79`). This information is necessary to delete the event:

```
~/D/mobilizon-poster$ DEBUG=* bin/deleter.js --event 79
  mobi:shared Returning a token (343 bytes) registered on 2022-11-17T20:24:02.634Z +0ms
  mobi:shared Returning server from settings https://mobilizon.libr.events/api +1ms
  mobi:shared Connecting to https://mobilizon.libr.events/api +0ms
  mobi:shared:IO Status: 200 +0ms
{ data: { deleteEvent: { __typename: 'DeletedObject', id: '79' } } }
~/D/mobilizon-poster$ 
```

A token expire quite frequently, so is not unusual have to re-execute `login.js` before every command.