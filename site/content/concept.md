---
date: 2021-04-07
layout: page
draft: false

title: "Librevents & Data Liberation"
description: "How and why recapturing data and republishing it is an act of resistance against surveillance capitalism."
images: [ 'https://libr.events/images/libr.rocket.png' ]
---

## `Data Liberation`

### 🩴 Data liberation is the act of reconquering data held hostage by big tech. By copying data, reprocessing it, and making them accessible through free protocols and networks. This is aimed at breaking their monopoly, and is an example of adversarial interoperability.

##### 🩴 It is a movement that empowers individuals and communities to take back their data and use it in ways that align with their values and goals. The more we participate in this global movement, the more the value of decentralized and free networks increases, where individuals and communities have more power over their data and where our data is not exploited by surveillance capitalism.


## `Librevents`

### 🚀 It is a browser extension that allows any user to copy (by "scraping") _details and invitations to events_ from proprietary platforms such as Facebook Events or Eventbrite. With one click, it also republishes these events on the Fediverse (the decentralized network). It is one of the first attempts of data liberation.

{{<extension-button
  firefox="https://addons.mozilla.org/en-US/firefox/addon/librevents/"
  chrome="https://chrome.google.com/webstore/detail/librevents/kjklnndemgmhlogoigglkpkaklenommj" >}}

##### 🚀 The data processed is initially posted as a “public event” by the organizer. We make this data truly “public” and available on free platforms, without violating the organizer’s original intentions. See how it works in this [30 minutes video](https://diode.zone/w/wy8nDtAQy7HRRtjdeJrLP7).

##### 🚀 The intention behind Librevents is to feed alternative ethical platforms like Mobilizon with content, in order to help them counter the “network effect” (users staying on Facebook because the information is only available there). The concept could later be applied to other types of contents and platforms. Read [how it works](/post/operational-flow-version-0.3.x/).


