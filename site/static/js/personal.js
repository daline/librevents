
function reportError(info) {
    $(".recipient").html('<h4>Fatal error: ' +
        info.message + '</h4>' +
        "<p>If this is your first access, you should perform some activity on Youtube before accessing here; <br/>and be sure extension is enabled from the switch in the popup (click on the icon). </p>"
    );
}

function getPubKey() {
    const t = window.location.href.split('/#').pop();
    if(t.length != 44 ) console.log("Wrong token length in the URL", t.length);
    return t;
}

function personal() {

    const pk = getPubKey();
    const urlp = buildApiUrl('personal', `${pk}`, 2);
    $.getJSON(urlp, (data) => {
        if(data.error) 
            reportError(data);
        else {
            /* this populated the first table */
            supporterInfo(
                data.supporter,
                _.map(data.metadata, 'posted').length,
                data.errors.length
            );
            /* and this an entry for each event */
            _.each(_.filter(data.metadata, 'posted'), appendEvent);
        }
    });
}

function supporterInfo(profile, postedn, errorn) {
    $("#first--access").text(profile.created);
    $("#last--access").text(profile.accessed);
    $("#events--liberated").text(postedn);
    $("#errors").text(errorn);
}

function optionalLink(href, text) {
    if(href) {
        return `<a target=_blank href="${href}">${text}</a>`
    } else
        return "";
}

function appendEvent(metadata) {
    const x = `<tr>
        <td>${metadata.when}</td>
        <td>${metadata.title}</td> 
        <td>${metadata.eventTime}</td> 
        <td>
            ${optionalLink(metadata.posted?.url, "Mobilizon")},
            ${optionalLink(metadata.href, "Original")},
            ${optionalLink(metadata.addressLink, "Organizer")}
        </td>
    </tr>`;

    $("#event--list").append(x);
    return;
}
