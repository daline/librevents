/* QA stays for:
 * Quality Assurance
 * and it implements an API to fetch date from the backend so we can
 * use the samples received to test improvement.
 * The file picked gets saved as "payload" or "mock" */
const _ = require('lodash');
const debug = require('debug')('lib:qa');
const activities = require('debug')('activities:qa');
const nconf = require('nconf');

const mongo = require('./mongo');

async function pickHTML(req, res) {
    /* this API should be cached and only every 10 minutes a new 
     * computation might occour. But, nope, premature optimization
     * is the root of all evil */
    const mongoc = await mongo.clientConnect();

    const offset = _.parseInt(req.params.offset);
    debug("Requested HTML %d", offset);

    /* the concept is to return only one HTML, ordered from the oldest, so it should be
     * consistent till the database gets deleted. */
    const monolist = await mongo.readLimit(mongoc, nconf.get('schema').htmls, {}, { savingTime: 1}, 1, offset);
    await mongoc.close()

    if(!monolist || !monolist.length) {
        activities("HTML offset %d is too big, returning error", offset);
        return { text: "Offset too big, HTML not present in the DB" };
    }

    const html = {
        ..._.pick(monolist[0], ['html', 'href', 'id', 'savingTime', 'update'])
    }
    activities("Returning HTML offset %d (from %s)", offset, html.savingTime);

    return { json: html };
}

module.exports = {
    pickHTML,
};
