const { describe, expect, test } = require('@jest/globals');
const _ = require('lodash');

const { samples, mockSupporter  } = require('../lib/test-utils');
const { processMINED, recursiveReassembly, dissectDates } = require('../lib/receiver');

describe('Test previously received Events', () => {

  test(`processMINED should not fault`, () => {
    _.each(samples, function(payload, i) {
      const result = processMINED(mockSupporter, payload);
      expect(result).toHaveProperty('id');
    });
  });

  test(`recursiveReassembly should produce text`, () => {
    _.each(samples, function(payload) {
      const description = recursiveReassembly(payload.textnest);
      expect(description).toHaveProperty('length');
      expect(description.length).toBeGreaterThan(10);
    });
  });

  test(`dissectDates actually works`, () => {
    _.each(samples, function(payload, i) {
      const dates = dissectDates(payload.date);
      expect(dates).toHaveProperty('start');
      expect(dates).toHaveProperty('end');
    });
  });

  test(`dissectDates with precise use cases`, () => {

    const case1 = "THURSDAY, NOVEMBER 17, 2022 AT 5:30 PM – 8:30 PM EST";
    let x = dissectDates(case1);

    const case2 = "JAN 28, 2023 AT 7:00 AM – JAN 29, 2023 AT 11:00 AM UTC+07";
    x = dissectDates(case2);

    const case3 = "SATURDAY, SEPTEMBER 3, 2022 AT 2:00 PM – 10:00 PM UTC+02";
    x = dissectDates(case3);

    const case4 = "NOV 17 AT 7:15 PM – NOV 19 AT 8:30 PM UTC-05";
    x = dissectDates(case4);

    // this is not yet complete, as the timezone and the AM/AP aren't checked
  });

});
