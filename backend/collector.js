const express = require('express');
const fs = require('fs');
const _ = require('lodash');
const { json, urlencoded } = require('body-parser');
const fourofour = require('debug')('librevents:404');
const debug = require('debug')('librevents:server');
const nconf = require('nconf');
const cors = require('cors');

const receiver = require('./lib/receiver');
const stats = require('./lib/stats');
const mongo = require('./lib/mongo');
const qa = require('./lib/qa');

const cfgFile = "./settings.json";
const redOn = "\033[31m";
const redOff = "\033[0m";

nconf.argv().env().file({ file: cfgFile });
console.log(`${redOn} ઉ nconf loaded, using ${cfgFile} ${redOff} — mongoDb: ${nconf.get('mongoDb')}`);

async function wrapRoutes(what, req, res) {
    if(what == 'input')
        return await receiver.processEvents(req, res);
    else if(what == 'output')
        return await receiver.returnEvent(req, res);
    else if(what == 'personal')
        return await receiver.personalContribs(req, res);
    else if(what == 'stats')
        return await stats.produce(req, res);
    else if(what == 'errors')
        return await stats.errors(req, res);
    else if(what == 'htmlretrieve')
        return await qa.pickHTML(req, res);
    else
        throw new Error("Developer Error: invalid API call");
}

async function iowrapper(what, req, res) {

    /* there are only two APIs at the moment,  */
    const httpresult = await wrapRoutes(what, req, res);

    if(_.isObject(httpresult.headers))
        _.each(httpresult.headers, function(value, key) {
            // debug("Setting header %s: %s", key, value);
            res.setHeader(key, value);
        });

    if(httpresult.json) {
        debug("API [%s] success, returning JSON <%d bytes>", what,
            _.size(JSON.stringify(httpresult.json)) );
        res.json(httpresult.json)
    } else if(httpresult.text) {
        debug("API [%s] success, returning text <%d bytes>", what,
            _.size(httpresult.text));
        res.send(httpresult.text)
    } else {
        debug("Undetermined return value from API [%s], → %j", what, httpresult);
        res.send(JSON.stringify(httpresult));
    }
};

/* configuration of express4 */
const app = express();
const LIMIT="4mb";
app.use(json({limit: LIMIT}));
app.use(urlencoded({extended: true}));
app.listen(nconf.get('port'), () => {
    debug("Binded sucessfully port %d", nconf.get('port'));
});

app.options('/api/v2/events', cors()) // enable pre-flight request for POST request as it has special headers
/* This POST only API, to collect the event HTML */
app.post('/api/v2/events', cors(), async function(req, res) {
    try {
        await iowrapper('input', req, res);
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'input': %s", error);
    };
});

/* This GET only API, returns the last 20 errors registered */
app.get('/api/errors/:key', cors(), async function(req, res) {
    const key = nconf.get('key');
    debug("Accessed admin only interface: %s | %s", key, req.params.key);
    try {
        if(key !== req.params.key || key.length < 2) {
            res.send("Invalid key");
        } else {
            await iowrapper('errors', req, res);
        }
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'errors': %s", error);
    };
});

/* This GET only API, to collect the event HTML */
app.get('/api/v2/events/:eventId', cors(), async function(req, res) {
    try {
        await iowrapper('output', req, res);
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'output': %s", error);
    }
});

app.options('/api/version', cors());
app.get('/api/version', cors(), async function(req, res) {
    try {
        const content = fs.readFileSync('package.json', 'utf-8');
        const info = JSON.parse(content);
        res.json({
            version: info.version
        })
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'version': %s", error);
    }
});

app.get('/api/htmls/:offset', cors(), async function(req, res) {
    try {
        await iowrapper('htmlretrieve', req, res);
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'htmlretrieve': %s", error);
    }
});

app.options('/api/v2/personal/:publiKey', cors());
app.get('/api/v2/personal/:publicKey', cors(), async function(req, res) {
    try {
        await iowrapper('personal', req, res);
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'personal' %s", error);
    }
});

app.options('/api/stats/', cors());
app.get('/api/stats/', cors(), async function(req, res) {
    /* This API do not support any parameter, it just return all the 
     * stats defined in https://0xacab.org/daline/librevents/-/issues/8 */
    try {
        await iowrapper('stats', req, res);
    } catch(error) {
        debug("iowrapper Trigger an Exception in 'stats': %s", error);
    }
});

/* Capture All 404 errors */
app.use(async (req, res, next) => {
    fourofour("invalid URL: %s", req.originalUrl);
    res
        .status(404)
        .send('Resource not found: check the code at https://0xacab.org/daline/librevents');
});

(async function() {
    try {
        await mongo.checkMongoWorks();
        debug("MongoDb connection works!");
    } catch(error) {
        console.log(`Fatal error: can't connect to database: ${error.message}, please check also ${cfgFile}`);
        process.exit(1);
    };
})();
